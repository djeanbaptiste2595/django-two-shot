from django.urls import path

from django.contrib.auth import views as auth_views
from receipts.views import (ReceiptListView, ReceiptCreateView)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
]
