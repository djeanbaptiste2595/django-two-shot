from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from receipts.models import Receipt
from django.contrib.auth.mixins import LoginRequiredMixin


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"  #change receipt_list to receipt/list

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")
